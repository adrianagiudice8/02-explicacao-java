package com.itau.filmes;

public class Filme {
	public String titulo;
	public int anoLancamento;
	public Genero genero;
	
	public Ator[] atores;
	
	public String toString() {
		String texto = titulo + "[" + anoLancamento + "]";
		texto += "\n";
		texto += genero;
		texto += "\n";
		
		for(Ator pastel : atores ) {
			texto += "- " + pastel + "\n";
		}
		
		return texto;
	}
}
